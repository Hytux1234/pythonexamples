import turtle
import random

anglechange = random.randint(-180, +180)
othervariable = random.randint(-30, 30)
anothervariable = random.randint(1, 1000)
red = 255, 0, 0


for i in range(1, 100):
    turtle.color(red)
    turtle.forward(20)
    turtle.left(45)
    turtle.forward(20)
    turtle.left(90)
    turtle.color(green)
    turtle.forward(20)
    turtle.left(45)
    turtle.left(anglechange)
    turtle.color(blue)
    turtle.forward(50)
    turtle.right(10)
    turtle.forward(-30)
    turtle.left(anglechange - 40)
